//CRUD Operations

//[SECTION] (CREATE) Inserting documents

//Insert one document
/*
    Syntax:
        db.collectionName.insertOne({Object});
*/

db.users.insertOne({
    "firstName": "Jane",
    "lastName": "DOe",
    "age": 25,
    "contact": {
        "phone": "123465789", // naka sctring for calculation purposes
        "email": "janedoe@mail.com"
    },
    "courses": ["CSS","JavaScript","Python"],
    "department": "none"
})



db.courses.insertOne({
    "courseName": "JavaScript 101",
    "price": "5000",
    "slot": "20",
    "description":"Intro to JavaScript",
    "isActive": true,
    "department": "none"
})

//Insert Many

/*
    Syntax:
        db.collectionName.insertMany([{objectA}, {objectB}])
*/

db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
            "phone": "123456",
            "email": "stephenhawking@mail.com",
        },
        "courses": ["Python", "React", "PHP"],
        "department": "none"
    },
    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
            "phone": "654321",
            "email": "neilarmstrong@mail.com",
        },
        "courses": ["React", "Laravel", "PHP"],
        "department": "none"
    },
    {
        "firstName": "neil",
        "lastName": "Paco",
        "age": 50,
        "contact": {
            "phone": "123456",
            "email": "neilpaco@mail.com",
        },
        "courses": ["Python", "React", "PHP"],
        "department": "none"
    },
    {
        "firstName": "Victor",
        "lastName": "Hugo",
        "age": 45,
        "contact": {
            "phone": "123456",
            "email": "victorhugo@mail.com",
        },
        "courses": ["HTML", "React", "JavaScript"],
        "department": "none"
    }
])

//[SECTION] (Read) Finding documents

/*
    Syntax:
        db.collectionName.find()

        - this allows us to find/return all decuments in a collection

        for specific lng
        use this:

        db.collectionName.find({"field": "value"})
*/

db.users.find()
db.users.find({"firstName": "Neil"})

//[SECTION] (Update) Updating documents
//Updating a single document

/*
    Syntax:
        db.collectionName.updateone({criteria}, {$set: {field:value}});
*/

db.users.updateOne(
    {"firstName": "Jane"},
        {
            $set: {
                "firstName": "Jane",
                "lastName": "Doe",
                "age": 25,
                "contact": {
                    "phone": "123456",
                    "email": "janedoe@mail.com"
                },
                "courses": ["CSS", "JavaScript", "Python"],
                "department": "Operations",
                "status": "active"
            }
        }
)

//Updating multiple documents
/*
        Syntax:
            db.colectionName.updateMany({criteria}, {$set: {field: value}})
*/

db.users.updateMany(

    {department: "none"},
    {
        $set: {department: "HR"}
    }

)

// deleting a single

db.users.deleteOne({

	"firstName": "Jane";
})